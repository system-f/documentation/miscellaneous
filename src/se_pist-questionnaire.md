---
title: "SINGLE ENGINE PISTON AEROPLANE ENDORSEMENT"
subtitle: "ENGINEERING, DATA AND PERFORMANCE QUESTIONNAIRE"
geometry: "left=3cm,right=3cm,top=2cm,bottom=2cm"
output: pdf_document
---


* Aeroplane Make:  ___________________________

* Aeroplane Model: ___________________________

* Name:            ___________________________

* ARN:             ___________________________

* Endorsed by:     ___________________________ *(signature/name)*

* Endorser ARN:    ___________________________

* Satisfactorily Completed on: ___________________________

## The Endorsement Questionnaire

To qualify for the issue of an aeroplane endorsement you must be able to fly the
aeroplane to an acceptable standard as well as demonstrate a level of knowledge
that satisfies the person conducting your endorsement that you have completed
'training in the operating limitations, procedures and systems of the type of aeroplane
for which the endorsement is sought'. (CAO 40.1.0. paragraph 4.3 Note 1).

This questionnaire should help you satisfy these knowledge requirements. It will do
so in a structured and efficient manner and so enhance safety and help reduce
costs.

The document will also serve as a ready reference for you in the future, particularly if
you do not fly regularly.

In any case, CASA recommends that both you and your instructor retain a copy of
the questionnaire for at least 12 months as proof of completion of training.

## How to Answer These Questions

You should use references such as Flight Manuals, Pilot Operating Handbooks and
theory texts, and make liberal use of notes and sketches. These should be
completed on the applicable page of the questionnaire.

To assist you, the layout of the questionnaire corresponds to the sections of most
Pilot Operating Handbooks.

Note that some questions may not apply to the aeroplane type on which you are
being endorsed. You should mark these 'N/A' (not applicable).

----

#### General Aircraft Data

1. Aircraft
  * What is the make, type and model of the aeroplane?
  * In which category (categories) is the aeroplane permitted to fly?

2. List the applicable airspeeds for the aeroplane type:
  * Vno
  * Vmax X/W
  * Va (design manoeuvre speed)
  * Vx (best climb angle)
  * Turbulence penetration speed
  * Vs
  * Vy (best climb rate)
  * Vfe (first extension)
  * Vlo1 (Ldg gear operation up)
  * Vle (Ldg gear extended)
  * Vlo2 (Ldg gear operation down)
  * Vne
  * maximum landing light operating speed
  * maximum load factor (flaps up)
  * maximum load factor (flaps down)

#### Emergency Procedures

3. Detail the emergency procedures for the following situations *(if applicable)*:
  * engine fire on the ground
  * engine failure after take-off
  * engine fire airborne
  * engine failure in the cruise
  * electrical fire on the ground
  * electrical fire in flight
  * cabin fire in flight
  * rapid depressurisation
  * emergency undercarriage extension procedure
  * the optimum glide speed for the aeroplane
  * propeller overspeed
  * waste gate failure

#### Normal Procedures

4. State, describe or detail:
  * the start sequence for cold and hot starts
  * the RPM used for checking:
    * the ignition system
    * the propeller governing system *(if applicable)*
    * the carburettor heat
  * the maximum RPM drop and RPM differential between magnetos when checking
the ignition switches
  * the use of cowl flaps if fitted
  * the climb power setting, IAS and fuel flow
  * a typical 65% power setting, TAS and fuel flow at 5000 ft pressure height
  * using the aeroplane flight manual or POH, calculate the endurance for the
aeroplane at 5000ft AMSL (ISA) with 65% power set
  * how the mixture is leaned out in the cruise.

#### Weight and Balance, and Performance

5. Weight and Balance, and Performance
  * Specify the correct values of
    * the maximum ramp weight
    * the maximum take-off weight
    * the maximum landing weight
    * the maximum Zero fuel weight
    * the maximum number of adult persons on board (POB)
    * the maximum baggage weight
    * the maximum fuel which can be carried with a full load of adult
passengers (80Kg/person) and maximum baggage weight
  * do any of the previous weight limitations vary from category to category?
  * if so what are the weight limitations of each category?
  * using the aeroplane flight manual, and a typical loading problem posed by
the endorser, determine the take-off weight and balance solution *(Maximum
take-off weight and C of G position)*, the amount of fuel that can be carried
and the endurance
  * calculate the take-off distance required at maximum take-off weight,
2500ft AMSL and OAT 30°C
  * the minimum landing distance at maximum landing weight for the
conditions of the previous typical loading problem

#### Fuel System, Fuel and Fluids

6. State or describe/sketch for the aircraft on the following page:
  * the correct grade of fuel
  * any approved alternate fuel
  * the location of fuel tanks and drain points
  * the total and usable fuel in each tank
  * the position of the fuel tank vents
  * whether the engine has a carburettor or fuel injection system
  * if applicable, the priming system and its use
  * location of the fuel boost/auxiliary pump and when it should be used
  * what conditions apply to tank selection for take-off, landing and cruise
  * when refuelling to less than full tanks, what restrictions apply and how is the
quantity checked
  * if applicable, the minimum and normal hydraulic fluid capacity
  * the correct grade of oil for the aeroplane
  * the minimum oil quantity before flight
  * the maximum quantity of oil
  * the maximum, minimum and normal engine oil pressures

#### Engine and Propeller Details

7. Answer the following:
  * What is the make/model of the engine?
  * What is the power output, and number of cylinders?
  * What is the take-off power setting and time limit?
  * What is the Maximum Continuous power?
  * Is the engine supercharged of turbo-charged?
  * What is the maximum MAP permitted?
  * If turbo-charged
    * is the type of waste gate fitted (Fixed, Manual or Automatic)?
    * is the procedure for operating the waste gate?
    * prevents the engine from being overboosted?
  * If supercharged
    * prevents the engine from being overboosted?
    * controls the MAP in the climb/descent?
  * Describe the propeller governing system.
  * If the oil pressure to the propeller dome is lost, does the propeller go into
coarse or fine pitch?

#### Airframe

8. Answer the following:
  * What type is the undercarriage system (fixed/retractable/tricycle/conventional)?
  * Which control surfaces can be trimmed?
  * Describe the flap actuating system
  * Describe the flap indicating system
  * What is the flap operating range?
  * Sketch the location of all exits
  * Describe/sketch the location of
    * landing/taxy lights
    * fresh air intakes
    * fuel caps
  * What is the wing span of the aeroplane?

#### Ancillary Systems

9. Answer the following questions:
  * What systems are hydraulically operated?
  * What procedures are followed when a hydraulic system failure is suspected?
  * How many brake applications would be expected from a fully pressurised brake
accumulator (if applicable)?
  * What are the sources of electrical power?
  * What is the DC system voltage?
  * Can an external power source be used? If so, what is the procedure?
  * Where are the battery and external power receptacle located?
  * How long can the battery supply emergency power?
  * Following an alternator/generator failure in flight, which non essential electric
services should be switched off?
  * If a stall warning device is fitted, is it electrical or mechanical?
  * How is the cockpit ventilated?
  * How is the cockpit heated?
  * If a fuel burning heater is installed, describe the method used to turn the heater
on and off and detail any limitations.
  * What is the fuel consumption of the heater?
  * Describe the pressurisation system (if applicable)
  * Show the location of the following safety equipment
    * fire extinguisher
    * ELT
    * torches
    * survival equipment
    * first aid kit

#### Flight Instruments

10. Answer user the following questions:
  * Where are the pitot head(s), static vent(s) and any water drain points for the
pitot/static system located?
  * Is there a pitot heat system fitted?
  * Is there an alternate static source fitted?
    * where is this located?
    * what is the purpose of this system?
    * if used, what effect does it have on the pressure instruments?
  * Which flight instruments are operated electrically?
  * Which flight instruments are gyroscopically operated?
  * Which instruments are operated by vacuum?
