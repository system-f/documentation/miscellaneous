# Miscellaneous Documentation

### During build the following strings are substituted:

* `${CI_PAGES_DOMAIN}`
* `${CI_PAGES_DOMAIN}`
* `${CI_PAGES_URL}`
* `${CI_PAGES_URL}`
* `${CI_PROJECT_TITLE}`
* `${CI_PROJECT_URL}`
* `${COMMIT_TIME}`
* `${GITLAB_USER_NAME}`
* `${GITLAB_USER_EMAIL}`
* `${CI_COMMIT_SHA}`
* `${CI_PROJECT_VISIBILITY}`

